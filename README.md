# Smiley Face Editor and Bank Caluculator

## NOTE
This was an assignment done for CS1400 at Utah State University, some code from the project was done by Chad Mano as a basic for the assignment, but near all of the code was completed by myself. I am only claiming ownership of what I have written, which is all, or near all, of the Python code.

## Manual
This program has two parts kept in the file assn12-task1.py, which is the smiley face editor, and assn12-task2.py, which is the bank calculator. Once you  run either there will be clear instructions on what to input.
