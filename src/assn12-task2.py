# Isaac Anderson
# CS 1400-1
# Assn 12 Task 2
# This program asks a user for input on bank information, and then lets the user pick what they would like to
# display and if they want to withdraw/deposit money.

import account


def main():
    id = eval(input("Enter an ID: "))
    balance = eval(input("Enter a balance: "))
    intRate = eval(input("Enter an annual interest rate (between 0 and 0.1): "))

    acc = account.Account(id, balance, intRate)

    run = True
    while run:
        if id > 0 and balance > 0 and intRate > 0 and intRate <= 0.1:
            print("(1): Display ID")
            print("(2): Display Balance")
            print("(3): Display Annual Interest Rate")
            print("(4): Display Monthly Interest Rate")
            print("(5): Display Monthly Interest")
            print("(6): Withdraw money")
            print("(7): Deposit money")
            print("(8): Exit")
            choice = eval(input("Select one of the options above (1-8): "))

            if choice == 1:
                print("ID is: " + str(id))
            elif choice == 2:
                print("Account balance is $" + str(acc.getBalance()))
            elif choice == 3:
                print("Annual interest rate is " + str(format(acc.getAnnualInterestRate(), "0.2%")))
            elif choice == 4:
                print("Monthly interest rate is " + str(format(acc.getMonthlyInterestRate(), "0.2%")))
            elif choice == 5:
                print("Monthly interest is $" + str(format(acc.getMonthlyInterest(), "0.2f")))
            elif choice == 6:
                withdraw = eval(input("How much would you like to withdraw? "))
                if withdraw > 0:
                    acc.withdraw(withdraw)
                else:
                    print("Bad input.")
            elif choice == 7:
                deposit = eval(input("How much would you like to deposit? "))
                if deposit > 0:
                    acc.deposit(deposit)
                else:
                    print("Bad input.")
            elif choice == 8:
                run = False
                break
            else:
                print("Bad input.")
        else:
            print("Bad input. Try again.")
            main()

    print("Thank you for visiting your account!")

main()