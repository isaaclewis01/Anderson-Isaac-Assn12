# Isaac Anderson
# CS 1400-1
# Assn 12 Task 1
# This program draws a smiley face, and then user can change the face to be a different emotion, have a different
# eye color, or change to be frowning or smiling.

# Import turtle and create a variable for turtle
import turtle
#tr = turtle.Turtle()

class Face:
    def __init__(self):
        self.__smile = True
        self.__happy = True
        self.__dark_eyes = True

    def draw_face(self):
        turtle.clear()
        self.__draw_head()
        self.__draw_eyes()
        self.__draw_mouth()
        turtle.hideturtle()

    def __draw_head(self):
        color = "Yellow"if self.is_happy else "Red"
        turtle.penup()
        turtle.goto(0, -100)
        turtle.begin_fill()
        turtle.fillcolor(color)
        turtle.pendown()
        turtle.circle(100)
        turtle.end_fill()

    def __draw_eyes(self):
        color = "Black" if self.is_dark_eyes else "Blue"
        turtle.color(color)
        turtle.penup()
        turtle.goto(-50, 0)
        turtle.begin_fill()
        turtle.pendown()
        turtle.circle(10)
        turtle.end_fill()
        turtle.penup()
        turtle.goto(50, 0)
        turtle.begin_fill()
        turtle.pendown()
        turtle.circle(10)
        turtle.end_fill()
        turtle.color("Black")

    def __draw_mouth(self):
        mood = 120 if self.is_smile else -240
        turtle.penup()
        turtle.goto(-40, -50)
        turtle.setheading(300)
        turtle.width(3)
        turtle.pendown()
        turtle.circle(50, mood)
        turtle.setheading(0)

    def is_smile(self):
        return True if self.__smile else False

    def is_happy(self):
        return True if self.__happy else False

    def is_dark_eyes(self):
        return True if self.__dark_eyes else False

    def change_mouth(self):
        self.is_smile = False if self.is_smile else True
        self.draw_face()

    def change_emotion(self):
        self.is_happy = False if self.is_happy else True
        self.draw_face()

    def change_eyes(self):
        self.is_dark_eyes = False if self.is_dark_eyes else True
        self.draw_face()


def main():
    face = Face()
    face.draw_face()

    done = False

    while not done:
        print("Change My Face")
        mouth = "frown" if face.is_smile else "smile"
        emotion = "angry" if face.is_happy else "happy"
        eyes = "blue" if face.is_dark_eyes else "black"
        print("1) Make me", mouth)
        print("2) Make me", emotion)
        print("3) Make my eyes", eyes)
        print("0) Quit")

        menu = eval(input("Enter a selection: "))

        if menu == 1:
            face.change_mouth()
        elif menu == 2:
            face.change_emotion()
        elif menu == 3:
            face.change_eyes()
        else:
            break

    print("Thanks for Playing")

    turtle.hideturtle()
    turtle.done()


main()
