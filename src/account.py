class Account:
    def __init__(self, id = 0, balance = 100, annualInterestRate = 0):
        self.__annualInterestRate = annualInterestRate
        self.__id = id
        self.__balance = balance

    def getId(self):
        return self.__id

    def getBalance(self):
        return self.__balance

    def getAnnualInterestRate(self):
        return self.__annualInterestRate

    def getMonthlyInterestRate(self):
        return self.__annualInterestRate / 12

    def getMonthlyInterest(self):
        monthlyInterest = self.getMonthlyInterestRate() / 100
        return self.__balance * monthlyInterest

    def setId(self, id):
        self.__id = id

    def setBalance(self, balance):
        self.__balance = balance

    def setAnnualInterestRate(self, rate):
        self.__annualInterestRate = rate

    def withdraw(self, amount):
        self.__balance -= amount

    def deposit(self, amount):
        self.__balance += amount